---
title: "Lord of the Rings: The Fellowship of the Ring (2001)"
date: 2020-08-16
posterURL: "https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_UX182_CR0,0,182,268_AL_.jpg"
infoURL: "https://www.imdb.com/title/tt0120737/"
---

* **Alex:** Defo 9/10.
* **Howard:** I remember not liking this movie but 5/5 for the memes.
* **Jordan:** 5/5 Nazguls served. No hobbits were harmed in the making of this rating.
* **Olivia:** I feel like I would have understood the movie better if I had read the book. But great movie over all. 9/10 would watch again.