---
title: "Lord of the Rings: The Return of the King (2003)"
date: 2020-08-30
posterURL: "https://m.media-amazon.com/images/M/MV5BNzA5ZDNlZWMtM2NhNS00NDJjLTk4NDItYTRmY2EwMWZlMTY3XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX182_CR0,0,182,268_AL_.jpg"
infoURL: "https://www.imdb.com/title/tt0167260/"
---