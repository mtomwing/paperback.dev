---
title: "Harry Potter and the Philosopher's Stone (2001)"
date: 2020-06-07
posterURL: "https://m.media-amazon.com/images/M/MV5BNjQ3NWNlNmQtMTE5ZS00MDdmLTlkZjUtZTBlM2UxMGFiMTU3XkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_UX182_CR0,0,182,268_AL_.jpg"
infoURL: "https://www.imdb.com/title/tt0241527/"
---