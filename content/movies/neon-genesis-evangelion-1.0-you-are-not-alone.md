---
title: "Neon Genesis Evangelion: 1.0 You Are (Not) Alone (2007)"
date: 2020-08-09
posterURL: "https://cdn.myanimelist.net/images/anime/7/74975.jpg"
infoURL: "https://myanimelist.net/anime/2759/Evangelion__10_You_Are_Not_Alone"
---